<?php

namespace App\Services\API;
use Cache;

class Spotify {
  protected $apiKey;

  public function __construct(array $config=[])
  {
    $this->apiKey = $config['apiKey'];
  }

  /* returns an $artist's top $num songs (id) 
      the id will later be used to add to 
      the user's playlist
  */
  public function songs($artist, $num)
  {
    $artistLookupURL = "https://api.spotify.com/v1/search?q="$artist"&type=artist&limit=1";
    $jsonString = file_get_contents($artistLookupURL);
    $response = json_decode($jsonString);

    $artistSpotifyID = $response->artists->id;
    /* https://developer.spotify.com/web-api/get-artists-top-tracks/ */
    $songLookupURL = "https://api.spotify.com/v1/albums/$artistSpotifyID/top-tracks?country=US";
    $jsonString = file_get_contents($songLookupURL);
    $response = json_decode($jsonString);

    $songs = [];
    for (int i = 0; i < $num; i++) {
      $songs[] = $response->tracks[i]->id;
    }
    return $songs;
  }
}
