# ITP 405 Final Project

### Project Starter
** What is your site about? **

My site will allow a user to view and/or create playlists associated with large music events (ex. Coachella).  

Home page will have recently created playlists.

User can search for an event to view a playlist.  Some results will yield multiple events so user will have to select from a drop down or something.

If it doesn't exist user can create a playlist.

Creation will allow user to select certain artists, max number of songs per artist

User can also search for other Spotify users that have used the website and see what they've created (only shows playlists created for website, not all of their playlists).

The following are tentative:

User can also search for an artist to view events/playlists they are related to

** What types of users will visit your site? **

Music lovers and Spotify users.

** Will they need to create an account? **

No, they will not need to create an account, but they will need a Spotify account.

** What API will you be using? **

Spotify (to find songs and create a playlist) & SeatGeek (to find events and the artists performing)

Only issue with SeatGeek (and most other event APIs) is you cannot search events by title, instead it will be a general query.  However, it not as restrictive as SongKick or TicketMaster. 

** What do you think your tables will be? **

Spotify User table -- user info (username, profile picture), playlists ids from playlist table

Playlist table -- playlist link, playlist info (title, description, date created)

Event table -- event info (title, location, date, etc), playlist ids from playlist table

Tentative:
Artist table -- playlist ids from playlist table