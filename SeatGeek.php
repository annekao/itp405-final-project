<?php

namespace App\Services\API;
use Cache;

class SeatGeek {


  /* finds events&performers based on a user's query
      returns all related events with the query
      in the title
  */
  public function events($q)
  {
    $lookupURL = "https://api.seatgeek.com/2/events?q=$q";

    $jsonString = file_get_contents($lookupURL);
    $response = json_decode($jsonString);

    $events = [];
    foreach($response->events as $event) {
      $lower = strtolower($event->title);
      if (strpos($lower, $q) !== false) {
        $events[] = $event;
      }
    }

    return $events;
  }
}
